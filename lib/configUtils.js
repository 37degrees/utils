import { Meteor } from 'meteor/meteor';
import _ from 'meteor/37degrees:utils/lib/init/_mixins';

export default class ConfigUtils {
  static get pfx () {
    return "[d37config] ";
  }

  static getObject (path, errorIfMissing) {
    if (!Meteor.settings) {
      console.log(ConfigUtils.pfx + "getString ERROR: Meteor.settings not found!");
      throw new Meteor.Error('app-config-error', 'getObject ERROR: Meteor.settings not found!');
    }

    var obj = Meteor.settings;

    // http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
    path = path.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    path = path.replace(/^\./, '');           // strip a leading dot
    var a = path.split('.');
    while (a.length) {
      var n = a.shift();
      if (n in obj) {
        obj = obj[n];
      } else {
        console.log(ConfigUtils.pfx + "getObject path not found: " + path);
        if (errorIfMissing) {
          throw new Meteor.Error('app-config-error', 'getObject path not found: ' + path);
        }
        return null;
      }
    }

    console.log(ConfigUtils.pfx + "getObject for: " + path);
    return obj;
  }

  static getBoolean (path, errorIfMissing) {
    var value = ConfigUtils.getObject(path, errorIfMissing);
    if (value === null) {
      return false;
    } else if (typeof value === 'string') {
      return value.toLowerCase() === 'true';
    } else if (typeof value === 'boolean') {
      return value;
    }
    return false;
  }

  static getString (path, errorIfMissing) {
    var obj = ConfigUtils.getObject(path, errorIfMissing);
    if (obj) {
      try {
        obj = obj.toString();
      }
      catch (err) {
        console.log(ConfigUtils.pfx + 'getString error: ' + err);
      }
      if (obj && typeof obj === 'string') {
        return obj;
      }
    }
    return null;
  }

  static getNumber (path, errorIfMissing) {
    var obj = ConfigUtils.getObject(path, errorIfMissing);
    if (obj && _.isNumber(obj)) {
      return obj;
    }
    return null;
  }

  static get isProdEnvironment () {
    if (Meteor.isServer) {
      if (typeof process !== 'undefined' && process.env && process.env.NODE_ENV) {
        return process.env.NODE_ENV.toLowerCase() !== 'development'
      }
      throw Meteor.Error(ConfigUtils.pfx + 'ERROR: "process.env.NODE_ENV" not found.');
    }
    throw Meteor.Error(ConfigUtils.pfx + 'ERROR: "isProdEnvironment" is not available to client.');
  }

  static get isDevEnvironment () {
    return !ConfigUtils.isProdEnvironment;
  }
}

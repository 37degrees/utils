import moment from 'moment-timezone';
import TimezonePicker from 'meteor/joshowens:timezone-picker';
import _ from 'meteor/37degrees:utils/lib/init/_mixins';

// region Date Utility methods
export class DateUtils {
  static get MonthNames () {
    return ['January', 'February', 'March', 'April', 'May', 'June', 'July',
            'August', 'September', 'October', 'November', 'December'];
  }

  static get DayNames () {
    return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  }

  static getStartOfDay (date, timezone) {
    date = date || new Date();
    return new Date(MomentUtils.getStartOfDay(date, timezone));
  }

  static getEndOfDay (date, timezone) {
    date = date || new Date();
    return new Date(MomentUtils.getEndOfDay(date, timezone));
  }

  static getEndOfDayPlusHours (hours, timezone) {
    let endOfDay = MomentUtils.getEndOfDay(null, timezone);
    return endOfDay.add(hours, 'hours').toDate();
  }

  static getDateFromToday (valueOut, unitsOut, timezone) {
    return new Date(MomentUtils.getDateFromToday(valueOut, unitsOut, timezone));
  }

  static getYesterday (timezone) {
    return new DateUtils.getDateFromToday(-1, 'day', timezone);
  }

  static getTimezone () {
    if (TimezonePicker && TimezonePicker.detectedZone) {
      return TimezonePicker.detectedZone();
    }

    //TODO: replace this with a setting?
    return 'US/Pacific';
  }

  static getHoursFromNow (hours, timezone) {
    return new Date(MomentUtils.getHoursFromNow(hours, timezone));
  }

  static getDiffInMins (date1, date2) {
    return DateUtils.getDiff(date1, date2);
  }

  static getDiff (date1, date2, units = 'minutes') {
    var mom1 = moment(date1),
      mom2 = moment(date2);
    return mom1.diff(mom2, units, true);
  }

  static getMonthName (date) {
    if (date) {
      return DateUtils.monthNames[date.getMonth()];
    }
    return null;
  }

  static isSameDate (date1, date2) {
    if (!_.isDate(date1) || !_.isDate(date2)) {
      return false;
    }

    var mom1 = moment(date1);
    return mom1.isSame(date2, 'day');
  }

  static getFormatted (date, format, timezone) {
    return MomentUtils.getFormatted(date, format, timezone);
  }

  static getShort24Hour (date, format, timezone) {
    return DateUtils.getShort(date, format, timezone, true);
  }

  static getShort (date, format, timezone, is24HourClock = false) {
    if (date) {
      let momentDt = moment(date),
        defaultFormat = is24HourClock ? 'H:mm' : 'h:mm a';

      if (momentDt) {
        if (!format && !momentDt.isSame(new Date(), 'day')) {
          defaultFormat = 'MMM D [@] ' + defaultFormat;
        }
        format = format || defaultFormat;
        return MomentUtils.getFormatted(date, format, timezone);
      }
    }

    return null;
  }

  static startOfWeek (number) {
    return MomentUtils.startOfWeek(number).toDate();
  }

  static startOfIsoWeek (number) {
    return MomentUtils.startOfIsoWeek(number).toDate();
  }

  static endOfWeek (number) {
    return MomentUtils.endOfWeek(number).toDate();
  }

  static endOfIsoWeek (number) {
    return MomentUtils.endOfIsoWeek(number).toDate();
  }
}
// endregion

// region Moment-specific methods
export class MomentUtils {
  static getLocalDate (date, timezone) {
    date = date || new Date();
    timezone = timezone || DateUtils.getTimezone();
    return moment(date).tz(timezone);
  }

  static getFormatted (date, format, timezone) {
    var dt = MomentUtils.getLocalDate(date, timezone);
    return dt.format(format);
  }

  static getStartOfDay (date, timezone) {
    date = date || new Date();
    timezone = timezone || DateUtils.getTimezone();
    return moment(date).tz(timezone).startOf('day');
  }

  static getEndOfDay (date, timezone) {
    date = date || new Date();
    timezone = timezone || DateUtils.getTimezone();
    return moment(date).tz(timezone).endOf('day');
  }

  static getDateFromToday (valueOut, unitsOut, timezone) {
    if (!valueOut || !unitsOut) {
      return null;
    }

    timezone = timezone || DateUtils.getTimezone();
    return moment().tz(timezone).add(valueOut, unitsOut);
  }

  static getHoursFromNow (hours, timezone) {
    let now = MomentUtils.getLocalDate(timezone);
    return now.add(hours, 'hours');
  }

  static week (number) {
    //console.log('MomentUtils.week:', moment());
    //console.log('MomentUtils.week:', moment().week());
    return _.isNumber(number) ? moment().week(number) : moment().week();
  }

  static isoWeek (number) {
    //console.log('MomentUtils.isoWeek:', moment());
    //console.log('MomentUtils.isoWeek:', moment().isoWeek());
    return _.isNumber(number) ? moment().isoWeek(number) : moment().isoWeek();
  }

  static startOfWeek (number) {
    return moment().week(number).startOf('week');
  }

  static startOfIsoWeek (number) {
    return moment().isoWeek(number).startOf('isoWeek');
  }

  static endOfWeek (number) {
    let week = MomentUtils.week(number);
    return week.endOf('week');
  }

  static endOfIsoWeek (number) {
    let week = MomentUtils.isoWeek(number);
    return week.endOf('isoWeek');
  }

  static startOfWeekByDate (date, firstDayOfWeek = 0) {
    let offset = firstDayOfWeek * -1;
    date = date || new Date();
    MomentUtils.getStartOfDay(date).day(offset);
  }
}
//endregion

import _ from 'lodash';

_.mixin({
  isEqualStringValue: function (value1, value2, isCaseInsensitive = false) {
    if (_.isString(value1) && _.isString(value2)) {

      if (!isCaseInsensitive) {
        return value1.toLowerCase() === value2.toLowerCase();
      }

      return value1 === value2;
    }

    return false;
  },
  isNotEmptyString: function (obj) {
    return !_.isUndefined(obj) && _.isString(obj) && obj.trim() !== '';
  },
  isPropEqualTo: function (obj, property, valueToCompare, ignoreStringCase = true) {
    let value = _.get(obj, property);

    value = _.isString(value) && ignoreStringCase ? value.toLowerCase() : value;
    valueToCompare = _.isString(valueToCompare) && ignoreStringCase ? valueToCompare.toLowerCase() : valueToCompare;

    return _.isEqual(value, valueToCompare);
  }
});

export default _;

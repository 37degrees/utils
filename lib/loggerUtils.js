import { Logger } from 'meteor/ostrio:logger';
import { LoggerConsole } from 'meteor/ostrio:loggerconsole';
import { LoggerMongo } from 'meteor/ostrio:loggermongo';
//TODO: implement Slack notifications
//import { SlackAPI } from 'meteor/khamoud:slack-api';

export default class LoggerUtils {
  constructor (props) {
    if (!this.logger) {
      if (props && (props.console || props.mongo)) {
        this.logger = new Logger();
        console.log("d37utils: initialized ostrio logger");

        if (props.console) {
          let filter = props.console.filter || LoggerUtils.defaultSettings.filter.console;
          new LoggerConsole(this.logger).enable({ enable: true, filter: filter, client: true, server: true });
        }

        if (props.mongo) {
          let settings = props.mongo.collectionName ? { collectionName: props.mongo.collectionName } : {},
              filter   = props.mongo.filter || LoggerUtils.defaultSettings.filter.mongo;

          // allow client to call, but not execute
          new LoggerMongo(this.logger, settings).enable({
            enable: true,
            filter: filter,
            client: false,
            server: true
          });
        }

        //TODO: log any client errors --- do this in app to preserve stack
        //if (Meteor.isClient) {
        //  // Store original window.onerror
        //  var _WoE       = window.onerror;
        //  window.onerror = function (msg, url, line) {
        //    Goin.log.error(msg, { file: url, onLine: line });
        //    if (_WoE) {
        //      _WoE.apply(this, arguments);
        //    }
        //  };
        //}

        //TODO: implement Slack notifications
        //if (Meteor.isServer) {
        //  this.slackInit(props);
        //}

      } else {
        this.setFallbackLogger();
        console.log("d37utils: set fallback logger");
      }
    }
  }

  static get defaultSettings () {
    return {
      filter: {
        console: ['info', 'debug', 'trace', 'warn', 'error', 'fatal', '*'],
        mongo:   ['warn', 'error', 'fatal']
      }
    }
  }

  //TODO: implement Slack notifications, maybe using a Slack bot user
  //slackInit (props) {
  //  let slack = {
  //    apiKey: _.get(props, 'props.slack.apiKey'),
  //    levels: _.get(props, 'props.slack.levels')
  //  };
  //
  //  if (slack.apiKey && slack.levels) {
  //    var emitter = function (level, message, data, userId) {
  //      if (_.includes(slack.levels, level)) {
  //        SlackAPI.chat.postMessage()
  //      }
  //    };
  //  }
  //}

  setFallbackLogger () {
    this.logger = {
      log:   function (level, message, meta) {
        var msg = '[FB_Core.ConsoleLogger] ';
        level   = level || 'Info';

        msg += level + ': ' + message;

        if (meta) {
          if (meta && typeof meta === 'object' && JSON.decycle) {
            meta = JSON.decycle(meta);
          }
          msg += ' ' + JSON.stringify(meta);
        }

        console.log(msg);
      },
      info:  function (message, meta) {
        this.log('Info', message, meta);
      },
      debug: function (message, meta) {
        this.log('Debug', message, meta);
      },
      trace: function (message, meta) {
        this.log('Trace', message, meta);
      },
      warn:  function (message, meta) {
        this.log('Warn', message, meta);
      },
      error: function (message, meta) {
        this.log('Error', message, meta);
      },
      fatal: function (message, meta) {
        this.log('Fatal', message, meta);
      }
    }
  }
}

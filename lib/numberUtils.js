import _ from 'meteor/37degrees:utils/lib/init/_mixins';

export default class NumberUtils {
  static randomNumber (min, max) {
    return Math.random() * (max - min) + min;
  }

  static randomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  static randomIntArray (min, max, count, allowDuplicates = true) {
    var array = [],
        int;

    while (array.length < count) {
      int = NumberUtils.randomInt(min, max);
      if (allowDuplicates || !_.includes(array, int)) {
        array.push(int);
      }
    }

    return array;
  }

  static getRandomArrayItem (array) {
    var idx;
    array = array || [];
    idx   = NumberUtils.randomInt(0, array.length - 1);
    return array[idx];
  }

  static getNumericTotal () {
    var args, total = 0;
    if (arguments) {
      args = Array.prototype.slice.call(arguments, 0);
      for (var i = 0; i < args.length; i++) {
        total += NumberUtils.getNumberOrZero(args[i]);
      }
    }
    // TODO: this rounds 0.005 to 0.01 --- is this the desired rounding for currency?
    return NumberUtils.getNumericValue(total);
  }

  static getNumberOrZero (value) {
    var number = NumberUtils.getNumberOrNull(value);
    return number || 0;
  }

  static getNumberOrNull (value) {
    if (isNaN(value)) {
      return null;
    }
    var number = parseFloat(value);
    return isNaN(number) ? null : number;
  }

  static getNumericValue (value, roundTo) {
    var amount = 0;

    if (NumberUtils.isNumber(value)) {
      amount = NumberUtils.getNumberOrZero(value);
    }

    // TODO: verify rounding for currency
    if (NumberUtils.isNumber(roundTo)) {
      // Assume we want rounding to the right of the decimal, not the left!
      roundTo = Math.abs(roundTo) * -1;
      return Math.round10(amount, roundTo);
    }

    return amount;
  }

  static getNumericValueAbs (value, roundTo) {
    var amount = NumberUtils.getNumericValue(value, roundTo);
    return Math.abs(amount);
  }

  static isNumber (value) {
    return !isNaN(value);
  }

  static getNumericValuesInString (value) {
    //var number = value.match(/[0-9]+(\.{0,1})?([0-9]{0,2})?/);
    var number = value.match(/([0-9])?(\.)?([0-9]{0,2})?/);
    //console.log('getNumericValuesInString', number);
    if (number && _.isArray(number) && number.length > 0) {
      return number[0];
    }
    return null;
  }

  static getIntegerValueInString (value) {
    var number = value.match(/[0-9]+/);
    //console.log('getNumericValuesInString', number);
    if (number && _.isArray(number) && number.length > 0) {
      return number[0];
    }
    return null;
  }
}

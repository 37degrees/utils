Package.describe({
  name:          '37degrees:utils',
  version:       '0.1.0',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.versionsFrom('1.3');

  api.use([
    'ecmascript',
    'joshowens:timezone-picker@0.1.2',
    'u2622:persistent-session@0.4.4',
    'ostrio:logger@1.1.2',
    'ostrio:loggermongo@1.1.3',
    'ostrio:loggerconsole@1.2.1'
    //TODO: implement Slack notifications
    //'khamoud:slack-api'
  ]);

  api.imply([
    'joshowens:timezone-picker',
    'u2622:persistent-session'
  ]);

  api.mainModule('lib.js', ['client', 'server']);
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('utils');
  api.addFiles('utilsTests.js');
});
